import unittest

import responses

import datawarehouse

DW_API = 'http://server/api/1'


class TestObjects(unittest.TestCase):
    """Test requests are performed correctly."""

    checkout_iid = 42
    build_iid = 911
    test_iid = 1337
    testresult_iid = 7

    checkout_id = 'redhat:9876543210'
    build_id = 'redhat:9876543210-x86_64-kernel'
    test_id = 'redhat:9876543210-x86_64-kernel_upt_10'
    testresult_id = 'redhat:9876543210-x86_64-kernel_upt_10.2'

    checkout = {
        "id": checkout_id,
        "misc": {"iid": checkout_iid}
    }
    build = {
        "id": build_id,
        "misc": {"iid": build_iid}
    }
    test = {
        "id": test_id,
        "misc": {"iid": test_iid}
    }
    testresult = {
        "id": testresult_id,
        "misc": {"iid": testresult_iid}
    }

    def setUp(self):
        self.dw = datawarehouse.Datawarehouse('http://server', 'my-secret-token')
        self.mock_responses()

    def mock_responses(self):
        """Mock all API calls."""
        def generic_list(grouped_by=None, id_key='id'):
            """Get a generic list of json objects."""
            g_list = [{id_key: 1}, {id_key: 2}]
            if grouped_by:
                g_list = {grouped_by: g_list}
            return {'results': g_list}

        get_endpoints = [
            ('/pipeline/123', {'pipeline_id': 123}),
            ('/issue', generic_list()),
            ('/issue/1', {'id': 1}),
            ('/issue/-/regex', generic_list()),
            ('/test', generic_list()),
            ('/test/2', {'id': 2}),
            ('/kcidb/data/checkouts?name=checkouts&last_retrieved_id=0&limit=1', self.checkout),
            ('/kcidb/data/builds?name=builds&last_retrieved_id=0&limit=1', self.build),
            ('/kcidb/data/tests?name=tests&last_retrieved_id=0&limit=1', self.test),
            ('/kcidb/checkouts', {'results': [self.checkout]}),
            (f'/kcidb/checkouts/{self.checkout_iid}', self.checkout),
            (f'/kcidb/checkouts/{self.checkout_id}/all', {'checkouts': [self.checkout],
                                                          'builds': [self.build],
                                                          'tests': [self.test]}),
            (f'/kcidb/checkouts/{self.checkout_id}/builds', {'results': [self.build]}),
            (f'/kcidb/checkouts/{self.checkout_id}/issues', generic_list()),
            (f'/kcidb/checkouts/{self.checkout_id}/issues/1', {'id': 1}),
            (f'/kcidb/checkouts/{self.checkout_id}/issues/occurrences', generic_list()),
            (f'/kcidb/checkouts/{self.checkout_id}/issues/occurrences/1', {'id': 1}),
            (f'/kcidb/checkouts/{self.checkout_id}/recipients',
             {'send_to': ['to@mail.com'], 'send_cc': ['cc@mail.com']}),
            (f'/kcidb/builds/{self.build_iid}', self.build),
            (f'/kcidb/builds/{self.build_id}/tests', {'results': [self.test]}),
            (f'/kcidb/builds/{self.build_id}/issues', generic_list()),
            (f'/kcidb/builds/{self.build_id}/issues/1', {'id': 1}),
            (f'/kcidb/builds/{self.build_id}/issues/occurrences', generic_list()),
            (f'/kcidb/builds/{self.build_id}/issues/occurrences/1', {'id': 1}),
            (f'/kcidb/tests/{self.test_iid}', self.test),
            (f'/kcidb/tests/{self.test_id}/issues', generic_list()),
            (f'/kcidb/tests/{self.test_id}/issues/1', {'id': 1}),
            (f'/kcidb/tests/{self.test_id}/issues/occurrences', generic_list()),
            (f'/kcidb/tests/{self.test_id}/issues/occurrences/1', {'id': 1}),
            (f'/kcidb/tests/{self.test_id}/results', {'results': [self.testresult]}),
            (f'/kcidb/testresults/{self.testresult_iid}', self.testresult),
            (f'/kcidb/testresults/{self.testresult_id}/issues', generic_list()),
            (f'/kcidb/testresults/{self.testresult_id}/issues/1', {'id': 1}),
            (f'/kcidb/testresults/{self.testresult_id}/issues/occurrences', generic_list()),
            (f'/kcidb/testresults/{self.testresult_id}/issues/occurrences/1', {'id': 1}),
        ]

        for endpoint, payload in get_endpoints:
            responses.add(responses.GET, DW_API + endpoint, json=payload)

        post_endpoints = [
            ('/kcidb/submit', {}),
            # issue occurrences
            (f'/kcidb/checkouts/{self.checkout_id}/issues', {'id': 2}),
            (f'/kcidb/builds/{self.build_id}/issues', {'id': 2}),
            (f'/kcidb/tests/{self.test_id}/issues', {'id': 2}),
            # last triaged at
            (f'/kcidb/checkouts/{self.checkout_id}/actions/triaged', {}),
            (f'/kcidb/builds/{self.build_id}/actions/triaged', {}),
            (f'/kcidb/tests/{self.test_id}/actions/triaged', {}),
            (f'/kcidb/testresults/{self.testresult_id}/actions/triaged', {}),
        ]

        for endpoint, payload in post_endpoints:
            responses.add(responses.POST, DW_API + endpoint, json=payload)

    @responses.activate
    def test_get_pipeline(self):
        """Test get pipeline."""

        pipeline = self.dw.pipeline.get(123)
        self.assertEqual(123, pipeline.pipeline_id)

        self.assertEqual(
            responses.calls[0].request.url,
            DW_API + '/pipeline/123'
        )

    @responses.activate
    def test_get_issue_regex(self):
        """Test issue_regex."""
        self.dw.issue_regex.list()

    @responses.activate
    def test_get(self):
        """Test tests."""
        self.dw.test.list()
        self.dw.test.get(2)

    @responses.activate
    def test_kcidb_data(self):
        """Test kcidb endpoints."""
        pagination = '&last_retrieved_id=0&limit=1'
        self.dw.kcidb.data.get(name='checkouts', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='builds', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='tests', last_retrieved_id=0, limit=1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/data/checkouts?name=checkouts' + pagination,
                DW_API + '/kcidb/data/builds?name=builds' + pagination,
                DW_API + '/kcidb/data/tests?name=tests' + pagination,
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_issues(self):
        """Test issues."""
        self.dw.issue.list()
        self.dw.issue.list(resolved=False)
        self.dw.issue.get(1)

        self.assertListEqual(
            [
                DW_API + '/issue',
                DW_API + '/issue?resolved=False',
                DW_API + '/issue/1',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_submit(self):
        """Test kcidb submit."""
        self.dw.kcidb.submit.create(data={'key': 'value'})
        self.assertEqual(
            [
                (DW_API + '/kcidb/submit', '{"data": {"key": "value"}}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb(self):
        """Test kcidb."""
        self.dw.kcidb.checkouts.list()
        rev = self.dw.kcidb.checkouts.get(self.checkout_iid)
        rev.builds.list()
        build = self.dw.kcidb.builds.get(self.build_iid)
        build.tests.list()
        test = self.dw.kcidb.tests.get(self.test_iid)
        test.results.list()
        self.dw.kcidb.testresults.get(self.testresult_iid)

        self.assertListEqual(
            [
                DW_API + '/kcidb/checkouts',
                DW_API + f'/kcidb/checkouts/{self.checkout_iid}',
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/builds',
                DW_API + f'/kcidb/builds/{self.build_iid}',
                DW_API + f'/kcidb/builds/{self.build_id}/tests',
                DW_API + f'/kcidb/tests/{self.test_iid}',
                DW_API + f'/kcidb/tests/{self.test_id}/results',
                DW_API + f'/kcidb/testresults/{self.testresult_iid}',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_issues(self):
        """Test kcidb issues."""
        rev = self.dw.kcidb.checkouts.get(self.checkout_iid)
        build = self.dw.kcidb.builds.get(self.build_iid)
        test = self.dw.kcidb.tests.get(self.test_iid)
        testresult = self.dw.kcidb.testresults.get(self.testresult_iid)

        responses.calls.reset()
        rev.issues.list()
        rev.issues.get(1)
        build.issues.list()
        build.issues.get(1)
        test.issues.list()
        test.issues.get(1)
        testresult.issues.list()
        testresult.issues.get(1)

        self.assertListEqual(
            [
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues',
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues/1',
                DW_API + f'/kcidb/builds/{self.build_id}/issues',
                DW_API + f'/kcidb/builds/{self.build_id}/issues/1',
                DW_API + f'/kcidb/tests/{self.test_id}/issues',
                DW_API + f'/kcidb/tests/{self.test_id}/issues/1',
                DW_API + f'/kcidb/testresults/{self.testresult_id}/issues',
                DW_API + f'/kcidb/testresults/{self.testresult_id}/issues/1',
            ],
            [call.request.url for call in responses.calls]
        )

        responses.calls.reset()
        rev.issues.create(issue_id=2)
        build.issues.create(issue_id=2)
        test.issues.create(issue_id=2)

        self.assertEqual(
            [
                (DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues', '{"issue_id": 2}'),
                (DW_API + f'/kcidb/builds/{self.build_id}/issues', '{"issue_id": 2}'),
                (DW_API + f'/kcidb/tests/{self.test_id}/issues', '{"issue_id": 2}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_checkout_action_triaged(self):
        """Test kcidb action triaged."""
        checkout = self.dw.kcidb.checkouts.get(self.checkout_iid)
        checkout.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + f'/kcidb/checkouts/{self.checkout_iid}', None),
                (DW_API + f'/kcidb/checkouts/{self.checkout_id}/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_build_action_triaged(self):
        """Test kcidb action triaged."""
        build = self.dw.kcidb.builds.get(self.build_iid)
        build.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + f'/kcidb/builds/{self.build_iid}', None),
                (DW_API + f'/kcidb/builds/{self.build_id}/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_test_action_triaged(self):
        """Test kcidb action triaged."""
        test = self.dw.kcidb.tests.get(self.test_iid)
        test.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + f'/kcidb/tests/{self.test_iid}', None),
                (DW_API + f'/kcidb/tests/{self.test_id}/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_testiresult_action_triaged(self):
        """Test kcidb action triaged."""
        testresult = self.dw.kcidb.testresults.get(self.testresult_iid)
        testresult.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + f'/kcidb/testresults/{self.testresult_iid}', None),
                (DW_API + f'/kcidb/testresults/{self.testresult_id}/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_checkout_recipients(self):
        """Test checkout recipients."""
        recipients = self.dw.kcidb.checkouts.get(self.checkout_iid).recipients.get()
        self.assertEqual(['to@mail.com'], recipients.send_to)
        self.assertEqual(['cc@mail.com'], recipients.send_cc)

    @responses.activate
    def test_checkout_all(self):
        """Test checkout all objects."""
        kcidb_file = self.dw.kcidb.checkouts.get(self.checkout_iid).all.get()

        self.assertEqual(
            [
                (DW_API + f'/kcidb/checkouts/{self.checkout_iid}', None),
                (DW_API + f'/kcidb/checkouts/{self.checkout_id}/all', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

        self.assertIsInstance(kcidb_file.checkouts[0], datawarehouse.objects.KCIDBCheckout)
        self.assertIsInstance(kcidb_file.builds[0], datawarehouse.objects.KCIDBBuild)
        self.assertIsInstance(kcidb_file.tests[0], datawarehouse.objects.KCIDBTest)

        # Make sure managers still work on these objects
        responses.calls.reset()
        kcidb_file.checkouts[0].issues.list()
        kcidb_file.builds[0].issues.list()
        kcidb_file.tests[0].issues.list()

        self.assertListEqual(
            [
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues',
                DW_API + f'/kcidb/builds/{self.build_id}/issues',
                DW_API + f'/kcidb/tests/{self.test_id}/issues',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_issues_occurrences(self):
        """Test kcidb issues occurrences."""
        rev = self.dw.kcidb.checkouts.get(self.checkout_iid)
        build = self.dw.kcidb.builds.get(self.build_iid)
        test = self.dw.kcidb.tests.get(self.test_iid)
        testresult = self.dw.kcidb.testresults.get(self.testresult_iid)

        responses.calls.reset()
        rev.issues_occurrences.list()
        rev.issues_occurrences.get(1)
        build.issues_occurrences.list()
        build.issues_occurrences.get(1)
        test.issues_occurrences.list()
        test.issues_occurrences.get(1)
        testresult.issues_occurrences.list()
        testresult.issues_occurrences.get(1)

        self.assertListEqual(
            [
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues/occurrences',
                DW_API + f'/kcidb/checkouts/{self.checkout_id}/issues/occurrences/1',
                DW_API + f'/kcidb/builds/{self.build_id}/issues/occurrences',
                DW_API + f'/kcidb/builds/{self.build_id}/issues/occurrences/1',
                DW_API + f'/kcidb/tests/{self.test_id}/issues/occurrences',
                DW_API + f'/kcidb/tests/{self.test_id}/issues/occurrences/1',
                DW_API + f'/kcidb/testresults/{self.testresult_id}/issues/occurrences',
                DW_API + f'/kcidb/testresults/{self.testresult_id}/issues/occurrences/1',
            ],
            [call.request.url for call in responses.calls]
        )
